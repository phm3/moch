#!/bin/bash
# author: phm3

# This scripts reads and records history of tracks played by the MOC player. 
# Everything is kept in a flat file

ARTIST="`mocp -i | grep 'Artist:' | sed -e 's/^.*: //'`"
SONG="`mocp -i | grep 'SongTitle:' | sed -e 's/^.*: //'`";
FILE="`mocp -i | grep 'File:' | sed -e 's/^.*: //'`";
echo -e $SONG"|"$ARTIST"|"$FILE >> ~/.moc/moc_history.txt



